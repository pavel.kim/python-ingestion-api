import requests
import yaml

class FactTable:

    def __init__(self, filename, file_header, parameters, transactionid, token) -> None:
        self.filename = filename
        self.header = file_header
        self.parameters = parameters
        self.transactionid = transactionid
        self.token = token

    @staticmethod
    def load_parameters(filename):
        return yaml.load(open(filename), Loader=yaml.FullLoader)

    @staticmethod
    def get_token(parameters):
        login_credentials = {"identifier": parameters["login"],
                             "password": parameters["pwd"]}
        req = requests.post("{url}:{port}/login".format(url=parameters["url"],
                                                        port=parameters["port"]), json=login_credentials)
        return req.json()['token']

    @staticmethod
    def open_transaction(parameters, token):
        r = requests.post("{}:{}/tables/{}/transaction/{}/from/official/test-comment".format(parameters["url"],
                                                                                             parameters["port"],
                                                                                             parameters["module"],
                                                                                             parameters["date"]),
                          headers={"Authorization": token})
        print("\n", "Transaction opened!")
        return r.text

    def load_fact_table(self):
        f = open(filename, 'rb')
        if self.header:
            next(f)
        r = requests.post("{}:{}/tables/{}/commits/{}/{}".format(self.parameters["url"],
                                                               self.parameters["port"],
                                                               self.parameters["module"],
                                                               transactionid,
                                                               self.parameters["date"]),
                          headers={"Authorization": token,
                                   "Content-Type": "text/csv"},
                          data=open(filename, 'rb'))
        print("\n", r.text)
        print("\n", r.status_code)
        print("\n", "Fact table inserted!")



    def progress_transaction(self):
        r = requests.post("{}:{}/tables/{}/progressTransaction/commits/{}/{}".format(self.parameters["url"],
                                                                                     self.parameters["port"],
                                                                                     self.parameters["module"],
                                                                                     self.transactionid,
                                                                                     self.parameters["date"]),
                          headers={"Authorization": token})
        print("\n", r.text)



    def rollback_transaction(self):
        r = requests.delete("{}:{}/tables/{}/transaction/{}/{}".format(self.parameters["url"],
                                                                       self.parameters["port"],
                                                                       self.parameters["module"],
                                                                       transactionid,
                                                                       self.parameters["date"]),
                          headers={"Authorization": token})
        print("\n", "Transaction rollbacked!")
        print("\n", r.text)

    def close_transaction(self):
        r = requests.post("{}:{}/tables/{}/closeTransaction/{}/{}/to/official".format(self.parameters["url"], self.parameters["port"], self.parameters["module"], transactionid, self.parameters["date"]),
                          headers={"Authorization": token})
        print("\n", "Transaction closed!")


if __name__ == "__main__":

    params = FactTable.load_parameters('params.yaml') #! change this
    filename = params["factfile"]
    token = FactTable.get_token(params)
    transactionid = FactTable.open_transaction(params, token)

    fact = FactTable(filename, False, params, transactionid, token) #! change this
    fact.load_fact_table()
    fact.progress_transaction()
    # fact.rollback_transaction()
    fact.close_transaction()
