import pandas as pd
import random, string
pd.set_option("display.max_columns", 100)
import numpy as np
from clickhouse_driver import Client
from datetime import datetime


host = "51.159.31.133"
port = 9000
c = Client(host=host, port=port, user='default_unlimited', password='thisIsADevPassword')

## Cleaning and formating the csv file to match with data schema
r = c.execute("SELECT * FROM ima_pilot2_Books LIMIT 1", with_column_types=True)
cols = [col_names[0] for col_names in r[-1]]
df = pd.read_csv("./demo_files/ima_pilot2_Books.csv")
for i in df.columns:
    if i not in cols:
        print(i)

for j in cols:
    if j not in df.columns:
        print(j)
df.drop(columns="ima_pilot2_Books", inplace=True)
# df.rename(columns={'Unnamed: 0': "ima_Books"}, inplace=True)
cols.remove("ima_pilot2_Books")
df = df[cols]

# cl = "2020-09-01,ptsbook,book,portfolio,globalaccountingtype,globalregulatorytype,region,localaccountingtype,localregulatorytype,filename,iscontributory,globalbusinessline,regionalbusinessline,businessline,desk,subdesk,portfoliobusinessline,global,regionlegalhrc,room,entity,subentitylevel1,subentitylevel2,regionptf,marketcode,locationcode,groupattribute1,groupattribute2,groupattribute3,groupattribute4,regionalattribute1,regionalattribute2,regionalattribute3,regionalattribute4,regionalattribute5,regionalattribute6,regionalattribute7,regionalattribute8,regionalattribute9,regionalattribute10,gblsubgroup,regulatory_fx,regulatory_ir,regulatory_cr,regulatory_ab,regulatory_eq,regulatory_sf,mi_fx,mi_ir,mi_cr,mi_ab,mi_eq,mi_sf,alco,financialdisclosure,limitmonitoring,branchsubsidiary,blexclude,volckertype,globalaccountingclassification,globalaccountinggroup,globalregulatoryclassification,globalregulatorygroup,localaccountingclassification,localaccountinggroup,localregulatoryclassification,localregulatorygroup".split(",")
# cols[0], cols[2] = cols[2], cols[0]
# cols.remove('ima_Books')
# df = df[cols]
df.sort_values(by="COBDate", ascending=True, inplace=True)
df.to_csv("./books.csv", index=False, header=False)

# Pnl
r = c.execute("SELECT * FROM ima_Pnl LIMIT 1", with_column_types=True)
cols = [col_names[0] for col_names in r[-1]]
df = pd.read_csv("./raw_files/pnl_raw.csv")
cl = "2020-09-01,sourcesystem,tradecode,tradelegid,ptsbook,holdingperiod,status,whatif,assetclasscode,riskcurvecode,baseline,currencycode,productcode,scenariosettype,rowid,ptscurvecode,pnltype,origriskcurrency,riskcurvesubcomponent,scenariosetname,isicvar,riskfactorgroup,liquidityhorizon,scenarioset,sourcesystemgroup,scenariopnlvectorvalue,counterpartycode,refobligor,ratings,sector,country,basemtm,cvarobligor,cvarproducttype,cvarrating,seniority,cvarseniority,cvarsector,reportingsector,eventtype,cvarcurrency,cvareventtype,isinsurance,cvaobligorattrib,obligorattrib,pra_assetclass,riskclass,risktype,scenarioregion,cvarliquidityhorizon,cvarstressregion,cvarsidovr,afstradecode,reportingcurrency,distributekey,comment,gridid,berid,filename"


## Investigation deduplication managed by Ingestion API
cx = [i for i in cols if i not in ['PTSBook']]
df[cx].drop_duplicates()
for i in df.index:
    if i not in ix:
        print(df.loc[i, :])

r = c.execute("SELECT * FROM ima_Books", with_column_types=True)
cols = [col_names[0] for col_names in r[-1]]

## Convert csv to other file types
df = pd.read_csv("./raw_files/books.csv")
df.to_parquet('./raw_files/books.parquet', index=False, engine='pyarrow')
df.to_json('./raw_files/books.json', orient='values', index=True)


import rafal