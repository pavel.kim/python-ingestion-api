import requests
from FactTable import FactTable

parameters = FactTable.load_parameters('params.yaml')
token = FactTable.get_token(parameters)
dicts = parameters["dicts"].split(",")
dictfiles = parameters["dictfile"].split(",")
content_types = {"csv": "text/csv", "parquet": "multipart/form-data", "json": "application/json"}

def load_dictionary(f, d):
    file = open(f, 'rb')
    file_type = f.split(".")[-1]
    r = requests.post("{}:{}/dictionaries/{}/{}".format(parameters["url"], parameters["port"], d, parameters["date"]),
                      headers={"Authorization": token, "Content-Type": content_types[file_type]},
                          data=file)
    print(r.text)
    print(r.status_code)
    print("\n", "Dictionary inserted!")

for i in range(len(dicts)):
    load_dictionary(dictfiles[i], dicts[i])