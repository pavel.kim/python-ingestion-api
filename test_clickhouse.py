from clickhouse_driver import Client
import pandas as pd
pd.set_option("display.max_columns", 100)


# hosts = ['51.159.1.193', '51.159.1.194', '51.159.1.186', '51.159.1.197'] # Demo scale=way
# hosts = ['163.172.154.1'] # Staging
hosts = ['51.159.31.133', '51.159.31.135'] # demo distributed
# hosts = ['172.19.0.2', '172.19.0.3', '172.19.0.4', '172.19.0.5'] # Local distributed
port = '9000'

# tables = ["ima_Pnl", "ima_Books", 'ima_CurrencyHierarchy', "ima_ProductHierarchy", "ima_StandardCurve"]
# dicts = ["ima_Books", 'ima_CurrencyHierarchy', "ima_ProductHierarchy", "ima_StandardCurve"]
# join_tables = ['ima_BooksJoin', 'ima_CurrencyHierarchyJoin', 'ima_ProductHierarchyJoin', 'ima_StandardCurveJoin']

tables = ["ima_pilot2_Pnl", "ima_pilot2_Books", "ima_pilot2_ProductHierarchy"]
dicts = ["ima_pilot2_Books", "ima_pilot2_ProductHierarchy"]
join_tables = ['ima_pilot2_BooksJoin', 'ima_pilot2_ProductHierarchyJoin']

def count_rows(hosts, table, fact=0):
    print("\n", f"Count of rows per server for table '{table}' and Join table if any':")
    for h in hosts:
        c = Client(host=h, port=port, user='default_unlimited', password='thisIsADevPassword')
        query = f"SELECT COUNT() FROM {table}"
        print(f"{h}: {int(c.execute(query)[0][0])}")
        if fact == 0:
            query = f"SELECT COUNT() FROM {table}Join"
            print(f"{h}: {int(c.execute(query)[0][0])}")

count_rows(hosts, 'ima_pilot2_Pnl', 1)
for i in range(len(dicts)):
    count_rows(hosts, dicts[i])

### Truncate tables ###

def truncate_dicts(hosts, table):
    print("\n", f"Truncate dictionary '{table}':")
    for h in hosts:
        c = Client(host=h, port=port, user='default_unlimited', password='thisIsADevPassword')
        c.execute(f"TRUNCATE TABLE shard_{table}Join")
        c.execute(f"SYSTEM RELOAD DICTIONARY default.{table}Join")
        c.execute(f"TRUNCATE TABLE shard_{table}Data")
        c.execute(f"SYSTEM RELOAD DICTIONARY default.{table}")
for j in dicts:
    truncate_dicts(hosts, j)

print("\n", f"Truncate table '{tables[0]}':")
for h in hosts:
    c = Client(host=h, port=port, user='default_unlimited', password='thisIsADevPassword')
    c.execute(f"TRUNCATE TABLE shard_{tables[0]}")


def display_rows(hosts, table):
    print("\n", f"Rows per server for table '{table}':")
    for h in hosts:
        c = Client(host=h, port=port, user='default_unlimited', password='thisIsADevPassword')
        query = f"SELECT * FROM {table}"
        r = c.execute(query, with_column_types=True)
        cols = [col_names[0] for col_names in r[-1]]
        df = pd.DataFrame(r[0], columns=cols)
        print(f"{h}:")
        # print(df[[table, "COBDate"]].head())
        print(df.head())

display_rows(hosts, "ima_pilot2_Pnl")
display_rows(hosts, "ima_pilot2_Books")
display_rows(hosts, "ima_CurrencyHierarchy")
display_rows(hosts, "ima_pilot2_ProductHierarchy")
display_rows(hosts, "ima_StandardCurve")

def display_join(hosts, table):
    print("\n", f"Rows per server for table '{table}':")
    for h in hosts:
        c = Client(host=h, port=port, user='default_unlimited', password='thisIsADevPassword')
        query = f"SELECT * FROM {table}"
        r = c.execute(query, with_column_types=True)
        cols = [col_names[0] for col_names in r[-1]]
        df = pd.DataFrame(r[0], columns=cols)
        print(f"{h}:")
        print(df.head())

display_join(hosts, "ima_Pnl")
display_join(hosts, "ima_BooksJoin")
display_join(hosts, "ima_CurrencyHierarchyJoin")
display_join(hosts, "ima_ProductHierarchy")
display_join(hosts, "ima_StandardCurve")