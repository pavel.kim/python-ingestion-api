def alphanumeric(k):
    return ''.join([random.choice(string.ascii_letters + string.digits) for i in range(k)]).upper()

